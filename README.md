# slick

[slick/slick](https://index.scala-lang.org/slick/slick) Scala Language Integrated Connection Kit (SQL)

* [*Scala Slick: Simple Example on Connecting to a PostgreSQL Database*
  ](https://queirozf.com/entries/scala-slick-simple-example-on-connecting-to-a-postgresql-database)
  2015-01 (about Slick 2.x)